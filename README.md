# rpi4rtems


## RTEMS on Raspberry Pi 4

Getting a good real-time operating system on the Raspberry Pi 4 seems like a good idea.  I have been following the work in progress on the Google GSoC project, and there is a very basic Board Support Package (BSP) implemented.  Thanks for the great start and follow on support.

Using this BSP I wanted to do all my work on a pair of Raspberry Pi's, one as the host development environment and the other as the target computer.  I built a nice development environment that can be used for either application development or BSP development.  The RTEMS and BSP can be fully compiled on a Raspberry Pi 4 8GB in a reasonable amount of time.  It takes every bit of that 8GB of memory and a little swap as well.  My first posting is a script that will build the development environment compiler and a small app.  It customizes the quick start guide specifically for the RPi4.

In the beginning I was copying the target executable code from the development host to a usb thumb drive then moving it over to the target computer.  I finally just set up a TFTP host on the development computer and now it is a simple matter of copying the executable to that directory.  The target will boot from either the USB drive or over the ethernet.  I will be working on the script to share on this repository that sets up the TFTP with a fixed ip address as my next task.  I'll also cover how to set the boot order in the target Pi.

I was originally using a simple USB to FT232H board for a JTAG connection, and a pair of wires for the serial interface directly on the RPi4.  It worked well, but I found a FT2232H board that has both JTAG and serial and I've moved over to that.  I will be sharing in a future post the wiring diagram and also the openOCD and gdb configurations and debugging start up steps for both boards.

The serial interface needed a display, I choose to use minicom, as it is simple and I bet I've used it for decades.  I will share the technique for finding the serial device and connecting.

In the end I have a nice clean hardware setup.  The software is easy to use (once you learn how).  My development environment is on the Ubuntu Desktop with a few terminal windows open.  One for minicom and the serial output.  A second with the openOCD connection.  The third is for the gdb debugging interface.  And I have a web page open so I can learn more.  Often I just ssh in to the host and on my desktop RPi4 I have the three terminals open with the browser.

In the future and hopefully with the help of the development community, I would like to try to:
    See if the Raspberry Pi Debug probe can be used with a Raspberry Pi 4 target
        would need help with the openOCD configuration files
    Try to get vsCode to integrate with the development tools set
        vsCode is my preferred IDE and I feel comfortable using it
    I would like to document the state of the ARM/SoC upon boot and learn how to use the:
        memory mapper
        interrupt controller
        DMA interface
    The first project in the BSP development that I'd like to try would be the I2C interface.

I am sharing this with the community so it can grow - both for application development as well as BSP improvement.

I consider the work in this repository to be creative commons and/or open source, and once I decide on the specific verions I'll include a copyright notice of that effect in the files.  Please feel free to contribute through gitlab merge requests or issues.


