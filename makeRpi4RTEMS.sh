#! /usr/bin/bash
#Create an RTEMS development environment on Ubuntu 64 for the Raspberry Pi 4

date

echo "First update your operating system"
sudo apt update -y
sudo apt upgrade -y

echo "Need to make python3 execute with the command python"
# We do this by adding a symbolic link to python3 from python
sudo ln -s /usr/bin/python3 /usr/bin/python

echo "Some additional files are needed to do the build and for the environment"
sudo apt install -y build-essential g++ gdb unzip pax bison flex texinfo \
 python3-dev python-is-python3 libncurses5-dev zlib1g-dev ninja-build pkg-config git curl minicom

echo "Creating the install directory called RPi4RTEMS"
# It will be created within your home directory
mkdir -p $HOME/RPi4RTEMS/src
cd $HOME/RPi4RTEMS/src
git clone git://git.rtems.org/rtems-source-builder.git rsb
git clone git://git.rtems.org/rtems.git

echo "Build/Install the Tool Suite (2 - 8 hours)"
echo "Be sure to close all other applications while this step is running"
cd $HOME/RPi4RTEMS/src/rsb/rtems
../source-builder/sb-set-builder --prefix=$HOME/RPi4RTEMS/rtems/6 6/rtems-aarch64

echo "Just checking to see if the tool set runs as expected"
# Reference section
$HOME/RPi4RTEMS/rtems/6/bin/aarch64-rtems6-gcc --version

echo "Build the Raspberry Pi 4 Board Support Package"
# Reference section 2.5
cd $HOME/RPi4RTEMS/src/rsb/rtems
../source-builder/sb-set-builder --prefix=$HOME/RPi4RTEMS/rtems/6 \
 --target=aarch64-rtems6 --with-rtems-bsp=aarch64/raspberrypi4b \
 --with-rtems-tests=yes 6/rtems-kernel


#####################Everything works down to this point
## why is the following indented?

echo "Testing of the BSP is skipped (section 2.6)"

echo "Build Your Application Section 2.7"
mkdir -p $HOME/RPi4RTEMS/app/hello
cd $HOME/RPi4RTEMS/app/hello
curl https://waf.io/waf-2.0.19 > waf
chmod +x waf
git init
git submodule add git://git.rtems.org/rtems_waf.git rtems_waf

echo "TEMPORARY FIX"
#need to implement a fix by changing branches in git
git -C rtems_waf checkout 68654b4


echo "Create source files and waf script file"
echo -e '/*
* Simple RTEMS configuration
*/

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
' > init.c

echo -e '/*
* Hello world example
*/

#include <rtems.h>
#include <stdlib.h>
#include <stdio.h>

rtems_task Init(
  rtems_task_argument ignored
)
{
  printf( "\\nHello World\\n" );
  exit( 0 );
}
' > hello.c

echo -e "
#
# Hello world Waf script
#
from __future__ import print_function

rtems_version = '6'

try:
    import rtems_waf.rtems as rtems
except:
    print('error: no rtems_waf git submodule')
    import sys
    sys.exit(1)

def init(ctx):
    rtems.init(ctx, version = rtems_version, long_commands = True)

def bsp_configure(conf, arch_bsp):
    # Add BSP specific configuration checks
    pass

def options(opt):
    rtems.options(opt)

def configure(conf):
    rtems.configure(conf, bsp_configure = bsp_configure)

def build(bld):
    rtems.build(bld)

    bld(features = 'c cprogram',
        target = 'hello.exe',
        cflags = '-g -O2',
        source = ['hello.c',
                  'init.c'])
" > wscript

echo "Building the application"
./waf configure --rtems=$HOME/RPi4RTEMS/rtems/6 --rtems-bsp=aarch64/raspberrypi4b
./waf

echo "Convert format and name for Raspberry Pi boot loader"
# See section 9.1.5
$HOME/RPi4RTEMS/rtems/6/aarch64-rtems6/bin/objcopy -Obinary $HOME/RPi4RTEMS/app/hello/build/aarch64-rtems6-raspberrypi4b/hello.exe $HOME/RPi4RTEMS/app/hello/build/aarch64-rtems6-raspberrypi4b/kernel8.img

echo "All done - copy file to SD card or TFTP folder"
#The file is in $HOME/RPi4RTEMS/app/hello/build/aarch64-rtems6-raspberrypi4b/kernel8.img
#TFTP will be set up in a different script
date

